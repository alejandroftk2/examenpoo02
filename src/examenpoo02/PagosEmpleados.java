/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpoo02;

/**
 *
 * @author elpep
 */
public class PagosEmpleados {
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private float pagoBaseHora;
    private int horasImpartidas;
    private int canHijos;

    public PagosEmpleados() {
    
        this.numDocente = 0;
        this.nombre = "";
        this.domicilio = "";
        this.nivel = 0;
        this.pagoBaseHora = 0.0f;
        this.horasImpartidas = 0;
        this.canHijos = 0;
        
    }

    public PagosEmpleados(int numDocente, String nombre, String domicilio, int nivel, float pagoBaseHora, int horasImpartidas, int canHijos) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoBaseHora = pagoBaseHora;
        this.horasImpartidas = horasImpartidas;
        this.canHijos = canHijos;
        
    }
    
    public PagosEmpleados(PagosEmpleados copia) {
        this.numDocente = copia.numDocente;
        this.nombre = copia.nombre;
        this.domicilio = copia.domicilio;
        this.nivel = copia.nivel;
        this.pagoBaseHora = copia.pagoBaseHora;
        this.horasImpartidas = copia.horasImpartidas;
        this.canHijos = copia.canHijos;
    
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoBaseHora() {
        return pagoBaseHora;
    }

    public void setPagoBaseHora(float pagoBaseHora) {
        this.pagoBaseHora = pagoBaseHora;
    }

    public int getHorasImpartidas() {
        return horasImpartidas;
    }

    public void setHorasImpartidas(int horasImpartidas) {
        this.horasImpartidas = horasImpartidas;
        
    }

    public int getCanHijos() {
        return canHijos;
    }

    public void setCanHijos(int canHijos) {
        this.canHijos = canHijos;
    }
    
    
    public float calcularPago(){
        float pagH = 0.0f;
        pagH = this.pagoBaseHora * this.horasImpartidas;
        return pagH;
    
    }
    public float calcularImpuesto(){
        float imp = 0.0f;
        imp = this.calcularPago() * 0.16f;
        return imp;
    
    }
    public float calcularBono() {
        float bono = 0.0f;
        
        if (canHijos >= 1 && canHijos <= 2) {
            bono = 0.05f; 
        } else if (canHijos >= 3 && canHijos <= 5) {
            bono = 0.10f;
        } else if (canHijos > 5) {
            bono = 0.20f; 
        }
        
        bono = bono * this.calcularPago();
        return bono;
        
    }
    
    public float calcularPagoTotal(){
        float tot = 0.0f;
        tot = this.calcularImpuesto() + this.calcularPago() + this.calcularBono();
        return tot;
    
    }
}
